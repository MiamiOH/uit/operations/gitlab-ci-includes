# GitLab CI Files

GitLab CI provides the ability to include external files in the `.gitlab-ci.yml` configuration. This capability allows us to centralize common configurations and organize them into easily reusable components.

https://docs.gitlab.com/ee/ci/yaml/#include

This project must be publicly available to allow GitLab to retrieve include files.

[[_TOC_]]

## Available Includes

The include files are organized into directories by major function.

### Deploy

Deploy jobs relate to producing and installing packages. This includes creating RPMs, copying them to the yum servers and triggering puppet runs to have the new RPM installed. Deploy tasks should be independent of the application framework.

### PHP

PHP jobs relate to applications written in PHP. Where possible, the jobs should be independent of any specific framework. Jobs which do expect specific framework features should be clearly documented.

The PHP directory also contains pipeline definitions, such as the Solution Delivery Laravel pipeline. This allows the application of a single, consistent pipeline to all Laravel projects.

## Project Workflow

We need to prevent breaking changes from causing all dependent jobs from failing. All use of these include files must be from release branches (i.e. 1.0, 2.0, 3.0 etc) rather than master or environment branches. Any breaking change should cause a new release branch to be created.

## Using Includes

These include files can used in a project's `gitlab-ci.yml` with the `include` keyword. For example:

```yaml
include:
  - project: 'MiamiOH/uit/operations/gitlab-ci-includes'
    ref: '3.0'
    file:
    - '/laravel-pipeline.yaml'
    - '/php-feature.yaml'
```

It is possible to create an entire pipeline using only includes.

Some jobs may expect one or more variables to be set. For example:

```yaml
variables:
  UNIT_TESTS_PATH: tests/Unit
```

Refer to each include file for details.

You may define your own project specific jobs to supplement jobs in the includes:

```yaml
unit-extra:
  stage: test
  script:
    - run some/extra/tests
```

Such jobs should be applied to an existing stage.

## Laravel Application

The most fully developed pipeline, as of this writing, is for a Laravel application.

**Please note**: By default, Laravel pipeline jobs will run using PHP 7.3 based images and assume an application targeted to that version of PHP. The Laravel pipeline requires specification of a PHP version using the `PHP_VERSION` variable in the `variables:` block once your application moves beyond PHP 7.3. For example `PHP_VERSION: '7.4'`. Currently, PHP versions `7.3`, `7.4`, and `8.1` are supported.

```yaml
include:
  - project: 'MiamiOH/uit/operations/gitlab-ci-includes'
    ref: '3.0'
    file: 
    # Include the templates for php jobs
    - '/php/php-job-templates.yaml'
    # Set up the standard Laravel pipeline
    - '/php/laravel-pipeline.yaml'
    # Include unit testing, analysis and feature testing jobs
    - '/php/php-unit.yaml'
    - '/php/php-analysis.yaml'
    - '/php/php-feature.yaml'
  # Run the php code quality assessment (coverage and CRAP score)
    - '/php/php-quality.yaml'
```
Include these lines to build and deploy a RPM
```yaml
    # Include jobs to build and deploy the RPM
    - '/deploy/rpm.yaml'
    - '/deploy/puppet.yaml'
 ```

### Laravel Application as a container

In addition to the Laravel Application lines above, to deploy a Laravel application as a container you must include  additional lines to your `.gitlab-ci.yml` file in your project. If desired, you may omit the lines above to _Include these lines to build and deploy a RPM_, but consider removing them after the container has been deployed.

Include these lines if you need to create images to deploy as containers:
```yaml
    # Included parameters used in following job definitions
    - '/build/k8svars.yaml'
    # Include the template for linting Docker files
    - '/docker/docker-lint.yaml'
    # Include jobs to build and publish the container
    - '/build/create-image.yaml'
    - '/deploy/container.yaml'
    # Include needed to cause helm to update the container with the new version of your application
    - '/apply/helm.yaml'
```

By default, the ci-includes **k8svars.yaml** file sets the variable `APP_NAME: $CI_PROJECT_NAME`; `CI_PROJECT_NAME` is a GitLab predefined variable that defaults to the project slug, i.e. for the project https://gitlab.com/MiamiOH/uit/soldel/development/laravel-CoolNewApp the default value for `CI_PROJECT_NAME` would be `laravel-CoolNewApp`. This value will define the naming scheme for the application's kubernetes namespace, containers, and destination web path. 

If your project slug name differs from the desired deployed application path (for example the slug has "laravel-" as a prefix) you must define the APP_NAME variable in your project's `.gitlab-ci.yml` file.

```yaml
variables:
  APP_NAME: 'CoolNewApp'
```

Please note that this `APP_NAME` value in the `.gitlab-ci.yml` file should match the value of `app_name` in the Kubernetes block described in step one of this document. Further, the `app_name` in terraform-kubernetes, terraform-aws and puppet projects should all match.

For each container image to be built for your application the pipeline expects a folder within the `container-build` directory for that image to contain a Docker file. If your application repository does not contain these folders, you must create them and the associated Docker files.
 
To initiate the build of an image, the image name must be defined in the gitlab-ci.yml file. 
```yaml
variables: 
  IMAGES:  'php-fpm' 
```
 If multiple images are to be built 
 ```yaml
 variables:
   IMAGES:  'php-fpm, nginx, artisan' 
```
 Additional images can be created by adding directories and an associated Dockerfile in the `container-build` directory. Then, an additional job would need to be defined in the pipeline.  

I.e. to create a swoop image for test the following job would create the image and make it available as a container.  
```yaml
deploy-swoop-test:
  extends:
    - .build-test
  variables: 
    tag: swoop
```   
or similar for production: 
```yaml
deploy-swoop-prod:
  extends:
    - .build-prod
  variables: 
    tag: swoop
```

## Security Jobs

### Secret Scanning

Ensuring a project's code does not contain any sensitive data is an essential goal for a code repository, and if it occurs we want to ensure they are detected and reported so the appropriate action can be taken. In order to scan your project's repository for secrets, add the following to your `gitlab-ci.yaml`:

```yaml
  include:
  - project: 'MiamiOH/uit/operations/gitlab-ci-includes'
    ref: '3.0'
    # Include the template for secret detection
    - '/security/secret-detection.yaml'
```

The base secret scanning jobs are provided by GitLab and leverage the [Gitleaks tool for secret detection](https://github.com/zricethezav/gitleaks). If you want to run this test locally, see the documentation for [local installation](https://github.com/zricethezav/gitleaks#getting-started) and [how to use the tool](https://github.com/zricethezav/gitleaks#usage).

#### Limitations of the Secret Scanning Jobs

Please be aware this is a not a comprehensive secret detection solution. If the `secret_evaluation` job fails, it already exists in the git history and appropriate post-exposure actions should be taken. Care should always be taken to ensure secrets and sensitive data is not committed to your repository. For more information, see [Gitlab's documentation on secret detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/).

## Docker File Linting

Many projects include a `Dockerfile`, including all projects being deployed to a container. To run a `Dockerfile` linting check job that also offers suggestions based on [Best practices for writing Dockerfiles](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/), you can include the following lines to your `gitlab-ci.yaml`:

```yaml
  include:
  - project: 'MiamiOH/uit/operations/gitlab-ci-includes'
    ref: '3.0'
    # Include the template for linting Docker files
    file: 
    - '/docker/docker-lint.yaml'
```

This job leverages the [Hadolint Dockerfile linter](https://github.com/hadolint/hadolint) and also has the benefit of running a lint check against any `bash` code inside the Dockerfile as well. The following variables are available for this job and can be specified in your `gitlab-ci.yaml` file:

* `HADOLINT_THRESHOLD`: The job will fail with an error threshold of `error` by default, other available options are `error`, `warning`, `info`, `style`, `ignore`, and `none`
* `PATH_TO_SEARCH`: This is the relative top level path the linter will search for `Dockerfile`s. The default value is `.`

**Please Note**: This job expects the Dockerfile to be named `Dockerfile` or `Dockerfile` with any desired suffix (e.g. `Dockerfile-dev`). If this naming convention is not adhered to the Dockerfile will not be checked.

## MU-Banner Deployment

At this time there are only a few steps in the pipeline for MU-Banner Deploys.

```yaml
include:
  - project: 'MiamiOH/uit/operations/gitlab-ci-includes'
    ref: '3.0'
    file:
    # Include the standard pipeline for mu-banner-deploy
    - '/mu-banner/mu-banner-pipeline.yaml'
    # Include the create-tar template file
    - '/prepare/create-tar.yaml'
    # If a shell file is in the project then this should also be included
    - '/shell/shell-analysis.yaml'
```

If the shell files are not located in the ``misc`` directory then that should be defined in the ``variables`` section with the parameter ``SHELL_LOCATION: misc ``

## Testing Project Builds

GitLab creates pipelines when a triggering event occur, such as pushing commits to branches. Unfortunately, you cannot simply edit a file and run the pipeline. You must commit the change to GitLab in order to test the result in the pipeline. This process is extremely inefficient when first setting up a pipeline or when troubleshooting a build failure. Fortunately, there is nothing preventing you from running most pipeline jobs in your local environment. (Jobs which require credentials to interact with remote systems are the exception.)

You can use Docker to run the appropriate container locally. The base form of the command is:

```
docker run -it --rm -v $(pwd):/src registry.gitlab.com/miamioh/uit/operations/build-images-pipeline/{image:tag} [{command}] [{arguments}]
```

The `-it` options result in an interactive TTY session. The `--rm` option removes the container when exited. The `-v` option mounts the given local directory to the specified path in the container. See `docker run --help` for more information.

The image is one of the images we maintain to support the pipeline. Most images have a default command which runs automatically. Some images take arguments which are passed to the command.

### Laravel Pipeline Jobs

The jobs run in a Laravel pipeline for deployment are unit and feature tests, prepare, build, deploy and apply.

The following steps show how to run these jobs. If you are troubleshooting a pipeline problem, you should not create a `.env` file to fully reproduce the job behavior.  The test and prepare jobs run `composer install` before the job script is executed. You should run `composer install` prior to the test jobs and `composer install --no-dev` prior to the prepare job.

The following sections refer to an `app.spec` file. That is the default name of the RPM spec file. Substitute the appropriate name if you have changed it in your project.

#### Unit and Feature Tests

The test jobs use PHPunit to run unit and feature tests. These jobs do not typically cause pipeline problems, aside from failed tests. The most likely problem is feature tests failing because of some inadvertent dependency on environment variables. Be sure to:

* Remove or comment out all contents of your `.env` file when testing these jobs.
* Change the version of PHP after `php:` to reflect the version of PHP your application uses

```
docker run -it --rm -v $(pwd):/src registry.gitlab.com/miamioh/uit/operations/build-images-pipeline/php:7.3-phpunit path/to/unit/tests
docker run -it --rm -v $(pwd):/src registry.gitlab.com/miamioh/uit/operations/build-images-pipeline/php:7.3-phpunit path/to/feature/tests
```

#### Prepare

The prepare job uses `ant` and the `build.xml` file to create the source distribution. The most common issue is the result tar file name. The produced file name must match the input file name for the following build job. Verify the names in the `build.xml` and `app.spec` files match.

Typically these would be:

```
build.xml: <property name="release_file" value="${package}-${version}"/>
app.spec: Source0:     %{name}-%{_version}.tar.gz
```

Note that these two files use very different syntax and variable names, you are only looking to validate the form of the name.

The prepare job requires a unique version identifier to function correctly. One possible source is the `CI_PIPELINE_ID` variable. You can provide any number you want for this value when running the image.

You can test the distribution job by running:

```
docker run -it --rm -v $(pwd):/src -e "CI_PIPELINE_ID=1" registry.gitlab.com/miamioh/uit/operations/build-images-pipeline/build:<php version>-dist-laravel -s app.spec
```

This command should result in a `dist` directory and appropriately named tar.gz file being created in your project. The file name is determined by the `build.xml` file.

#### Build

The build job takes a tar.gz file produced by the prepare job and uses the `app.spec` file to produce an RPM of the source. As with the prepare job, the most common issues is the expected tar file name. Review the prepare job notes to address that.

The build job requires the same unique version identifier used in the prepare job. It uses the `app.spec` file to determine the source file name to use.

You can test the creation of rpm in the job by running:

```
docker run -it --rm -v $(pwd):/src -e "CI_PIPELINE_ID=1" registry.gitlab.com/miamioh/uit/operations/build-images-pipeline/build:create-rpm -s app.spec
```

#### Deploy and Apply

These jobs are much less likely to be the cause of pipeline problems. They also rely on secret credentials to connect to the yum server or Foreman API and therefore are not easily run locally. If you have any trouble with these jobs, please ask Application Operations for assistance and reference the failed job.

## Terraform

`common.yaml` provides common entry names that terraform jobs can extend. This provides a common framework for AWS related terraform projects. This is the standard validate, fmt, plan, apply workflow. It also provides common paths for common directory structures for our environments.

`azure-ad.yaml` is specific to Azure AD deployments.

### Example

```yaml
include:
  - project: 'MiamiOH/uit/operations/gitlab-ci-includes'
    ref: '3.0'
    file: '/terraform/common.yaml'

# full workflow with UIT/production only
validate-UIT/production:
  extends: [.UIT/production, .validate]
fmt-UIT/production:
  extends: [.UIT/production, .fmt]
plan-UIT/production:
  extends: [.UIT/production, .plan]
apply-UIT/production:
  extends: [.UIT/production, .apply]
  dependencies: [plan-UIT/production]
  only: [production]
```
