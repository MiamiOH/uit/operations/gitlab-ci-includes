<#
.SYNOPSIS
Runs PSSsriptAanlyzer checks against parsing and error level errors in scripts
.DESCRIPTION
Long description

.NOTES
Original Author: Bryan Powell
#>
Describe "Errors" {
    # These would go in BeforeDiscovery { } when that gets implemented
    $ScriptsToTest = Get-ChildItem -Recurse Scripts/*.ps1
    $ScriptAnalyzerRulesError = Get-ScriptAnalyzerRule -Severity Error
  
    Context "Parsing" {
      # Populate hash table with each script
      $ParseLevelCases = foreach ($Script in $ScriptsToTest) {
        @{ Script = $Script }
      }
      It "Should not return any parsing level errors" -TestCases $ParseLevelCases {
        Invoke-ScriptAnalyzer -Path $Script -Severity ParseError | Should -BeNullOrEmpty
      }
    }
    Context "Error" {
      # Populate hash table with each error level error and each script
      $ErrorLevelCases = foreach ($Rule in $ScriptAnalyzerRulesError ) {
        foreach ($Script in $ScriptsToTest) {
          @{
            Script = $Script
            Rule   = $Rule
          }
        }
      }
      It "Should not return any error level violations for the <Rule> rule in <Script>" -TestCases $ErrorLevelCases {
        param($Script, $Rule)
        # Save script results in a variable
        $ScriptAnalyzerResults = Invoke-ScriptAnalyzer -Path $Script -IncludeRule $Rule.RuleName
        # Check results and write out if there is an error
        if ($ScriptAnalyzerResults) {
          $ScriptAnalyzerResultsString = $ScriptAnalyzerResults | Out-String
          Write-Warning $ScriptAnalyzerResultsString
        }
        # If no error (null or empty) test passes
        $ScriptAnalyzerResults | Should -BeNullOrEmpty
      }
    }
  }
  