<#
.SYNOPSIS
Runs PSSsriptAanlyzer checks against warning and information level errors in scripts
.DESCRIPTION
Long description

.NOTES
Original Author: Bryan Powell
#>
Describe "Quality" {
    # These would go in BeforeDiscovery { } when that gets implemented
    $ScriptsToTest = Get-ChildItem -Recurse Scripts/*.ps1
    $ScriptAnalyzerRulesWarning = Get-ScriptAnalyzerRule -Severity Warning
    $ScriptAnalyzerRulesInformation = Get-ScriptAnalyzerRule -Severity Information
  
    Context "Warning" {
      # Populate hash table with each warning level error and each script
      $WarningLevelCases = foreach ($Rule in $ScriptAnalyzerRulesWarning) {
        foreach ($Script in $ScriptsToTest) {
          @{
            Script = $Script
            Rule   = $Rule
          }
        }  
      }
      It "Should not return any warning level violations for the <Rule> rule in <Script>" -TestCases $WarningLevelCases {
        param($Script, $Rule)
        # Save script results in a variable     
        $ScriptAnalyzerResults = Invoke-ScriptAnalyzer -Path $Script -IncludeRule $Rule.RuleName
        # Check results and write out if there is an error
        if ($ScriptAnalyzerResults) {
          $ScriptAnalyzerResultsString = $ScriptAnalyzerResults | Out-String
          Write-Warning $ScriptAnalyzerResultsString
        }
        # If no error (null or empty) test passes
        $ScriptAnalyzerResults | Should -BeNullOrEmpty
      }
    }
    Context "Information" {
      # Populate hash table with each warning level error and each script
      $InformationLevelCases = foreach ($Rule in $ScriptAnalyzerRulesInformation) {
        foreach ($Script in $ScriptsToTest) {
          @{
            Script = $Script
            Rule   = $Rule
          }
        }  
      }
      It "Should not return any information level violations for the <Rule> rule in <Script>" -TestCases $InformationLevelCases {
        param($Script, $Rule)
        # Save script results in a variable     
        $ScriptAnalyzerResults = Invoke-ScriptAnalyzer -Path $Script -IncludeRule $Rule.RuleName
        # Check results and write out if there is an error
        if ($ScriptAnalyzerResults) {
          $ScriptAnalyzerResultsString = $ScriptAnalyzerResults | Out-String
          Write-Warning $ScriptAnalyzerResultsString
        }
        # If no error (null or empty) test passes
        $ScriptAnalyzerResults | Should -BeNullOrEmpty
      }
    }
  }
  