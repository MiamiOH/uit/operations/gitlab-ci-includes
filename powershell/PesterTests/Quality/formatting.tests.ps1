<#
.SYNOPSIS
Runs PSSsriptAanlyzer checks against warning and information level errors in scripts
.DESCRIPTION
Long description

.NOTES
Original Author: Bryan Powell
#>
Describe "Formatting" {
    # These would go in BeforeDiscovery { } when that gets implemented
    $ScriptsToTest = Get-ChildItem -Recurse Scripts/*.ps1
  
    Context "CodeFormatting" {
      # Populate hash table with each script
      $FormattingLevelCases = foreach ($Script in $ScriptsToTest) {
        @{ Script = $Script }
      }
      It "Should not return any parsing level errors" -TestCases $FormattingLevelCases {
        # Invoke-ScriptAnalyzer -Path $Script -Settings CodeFormatting | Should -BeNullOrEmpty
        param($Script)
        # Save script results in a variable
        $ScriptAnalyzerResults = Invoke-ScriptAnalyzer -Path $Script -Settings CodeFormatting
        # Check results and write out if there is an error
        if ($ScriptAnalyzerResults) {
          $ScriptAnalyzerResultsString = $ScriptAnalyzerResults | Out-String
          Write-Warning $ScriptAnalyzerResultsString
        }
        # If no error (null or empty) test passes
        $ScriptAnalyzerResults | Should -BeNullOrEmpty
      }
    }
  }
  